using Xunit;
using Task = System.Threading.Tasks.Task;
using HttpStatusCode = System.Net.HttpStatusCode;
using ArgumentException = System.ArgumentException;
using HttpRequestException = System.Net.Http.HttpRequestException;

namespace MMTDigital.Test
{
	using Domain.Models;
	using Domain.Services;

	public static class CustomerServiceTest
	{
		#region Private
		//SDW: Linux style for remote CI/CD!
		private const string TestUsersJsonFile = "../../../TestFiles/test-users.json";
		#endregion

		[Fact]
		internal static async Task GetOrderAsync_NonExistUser_Throws()
		{
			//Arrange
			var customerService = new CustomerServiceMock(TestUsersJsonFile);
			var customerRequest = new CustomerRequest { User = "non-exist@mmtdigital.co.uk" };

			//Act
			var exception = await Record.ExceptionAsync(async () => await customerService.GetOrderAsync(customerRequest));

			//Assert
			Assert.IsType<HttpRequestException>(exception);
			Assert.Equal(HttpStatusCode.NotFound, ((HttpRequestException)exception).StatusCode);
		}

		[Fact]
		internal static async Task GetOrderAsync_BadMatchUser_Throws()
		{
			//Arrange
			var customerService = new CustomerServiceMock(TestUsersJsonFile);
			var customerRequest = new CustomerRequest
			{
				User = "bob@mmtdigital.co.uk",
				CustomerID = "bad-match"
			};

			//Act
			var exception = await Record.ExceptionAsync(async () => await customerService.GetOrderAsync(customerRequest));

			//Assert
			Assert.IsType<ArgumentException>(exception);
		}

		[Theory]
		[InlineData("santa@north-pole.lp.com", "XM45001")]
		[InlineData("sneeze@fake-customer.com", "A99001")]
		[InlineData("cat.owner@mmtdigital.co.uk", "C34454")]
		[InlineData("dog.owner@fake-customer.com", "R34788")]
		internal static async Task GetOrderAsync_TestUsers_NoThrow(string user, string customerID)
		{
			//Arrange
			var customerService = new CustomerServiceMock(TestUsersJsonFile);
			var customerRequest = new CustomerRequest { User = user, CustomerID = customerID };

			//Act
			var exception = await Record.ExceptionAsync(async () => await customerService.GetOrderAsync(customerRequest));

			//Assert
			Assert.Null(exception);
		}
	}
}