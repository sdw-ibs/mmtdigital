using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenApiInfo = Microsoft.OpenApi.Models.OpenApiInfo;
using IWebHostEnvironment = Microsoft.AspNetCore.Hosting.IWebHostEnvironment;

namespace MMTDigital.Api
{
	using Domain.Services;
	using Context = Data.Context;
	using CustomerDetailsApi = Domain.Settings.CustomerDetailsApi;

	internal sealed class Startup
	{
		#region Private
		private readonly IConfiguration Configuration;

		private readonly IWebHostEnvironment Environment;

		//SDW: Returns root Swagger XML example file for type <T>
		private string GetAssemblyXml<T>()
		{
			var assemblyName = typeof(T).Assembly.GetName().Name;

			return $@"{Environment.ContentRootPath}\..\{assemblyName}\{assemblyName}.xml";
		}
		#endregion

		public Startup(IConfiguration configuration, IWebHostEnvironment environment) =>
			(Configuration, Environment) =
				(configuration, environment);

		public void ConfigureServices(IServiceCollection services) =>
			services
				.AddScoped<ICustomerService, CustomerService>()
				.Configure<CustomerDetailsApi>(x => Configuration.GetSection(nameof(CustomerDetailsApi)).Bind(x))
				.AddDbContext<Context>(x => x.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")))
				.AddSwaggerGen
				(
					x =>
					{
						x.IncludeXmlComments(GetAssemblyXml<ICustomerService>());
						x.SwaggerDoc("v1", new OpenApiInfo { Title = nameof(MMTDigital), Version = "v1" });
					}
				)
				.AddControllers();

		public void Configure(IApplicationBuilder application)
		{
			if (Environment.IsDevelopment())
				application
					.UseDeveloperExceptionPage()
					.UseSwagger()
					.UseSwaggerUI(x => x.SwaggerEndpoint("/swagger/v1/swagger.json", $"{nameof(MMTDigital)} v1"));

			application
				.UseRouting()
				.UseEndpoints(x => x.MapControllers());
		}
	}
}