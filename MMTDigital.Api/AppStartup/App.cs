using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace MMTDigital.Api
{
	internal static class App
	{
		private static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(x => x.UseStartup<Startup>());

		private static void Main(string[] args) =>
			CreateHostBuilder(args)
				.Build()
				.Run();
	}
}