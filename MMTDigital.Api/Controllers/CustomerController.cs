﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MMTDigital.Api.Controllers
{
	using Domain.Models;
	using ICustomerService = Domain.Services.ICustomerService;

	[ApiController]
	[Route("[controller]")]
	public sealed class CustomerController : ControllerBase
	{
		#region Private
		private readonly ICustomerService CustomerService;
		#endregion

		public CustomerController(ICustomerService customerService) =>
			CustomerService = customerService;

		[HttpPost]
		[HandleException]
		public async Task<CustomerResponse> Get(CustomerRequest request) =>
			await CustomerService.GetOrderAsync(request);
	}
}