﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using HttpStatusCode = System.Net.HttpStatusCode;
using JsonResult = Microsoft.AspNetCore.Mvc.JsonResult;
using HttpRequestException = System.Net.Http.HttpRequestException;

namespace MMTDigital.Api
{
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	internal sealed class HandleExceptionAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuted(ActionExecutedContext context)
		{
			var exception = context.Exception?.GetBaseException();

			if (exception != null)
			{
				var jsonException = new
				{
					Type = exception.GetType().Name,
					exception.Message

					//SDW: Could add further stack frame info here etc...
				};

				//SDW: Re-format exception result to simply show base message and type
				context.Result = new JsonResult(jsonException)
				{
					StatusCode = (exception is HttpRequestException httpRequestException) ?
						(int)httpRequestException.StatusCode :
						(int)HttpStatusCode.InternalServerError,
				};
				context.ExceptionHandled = true;
			}

			base.OnActionExecuted(context);
		}
	}
}