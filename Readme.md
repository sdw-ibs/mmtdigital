# MMT Senior Server Side .Net Technical Test
### Solution Features
* Developed using Microsoft Visual Studio Community 2019 (Version 16.9.3)
* SQL Server Management Studio (v18.8)
* Multi-layered  Architecture comprised of the following:
    * MMTDigital.Api (ASP.NET5 Core Web API)
    * MMTDigital.Data (.NET5 Data Access Library)
    * MMTDigital.Domain (.NET5 Domain Library)
    * MMTDigital.Test (.NET5 xUnit Project)
* Swagger Documentation (with examples)
* CI/CD Pipeline
* User Secrets
### Further Improvements
* Global Authentication and deployment of more secure JWT Bearer Tokens
* Utilization of .NET Core's logging features
* Enhanced security via enforcement of CORS policies
* Detailed stack frame info during exception handling
* Use of better mocking facilities, such as in-memory database contexts
* A lot more unit tests!
* More useful in-code comments etc.