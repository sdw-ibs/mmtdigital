﻿using InternalsVisibleToAttribute = System.Runtime.CompilerServices.InternalsVisibleToAttribute;

//SDW: Make assembly internals visible to unit testing project
[assembly: InternalsVisibleTo(nameof(MMTDigital) + ".Test")]