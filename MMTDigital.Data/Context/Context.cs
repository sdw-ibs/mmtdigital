﻿using Microsoft.EntityFrameworkCore;

namespace MMTDigital.Data
{
	using Models;

	public sealed class Context : DbContext
	{
		public Context(DbContextOptions<Context> options) : base(options)
		{
		}

		public DbSet<Orders> Orders { get; set; }

		public DbSet<Products> Products { get; set; }

		public DbSet<OrderItems> OrderItems { get; set; }
	}
}