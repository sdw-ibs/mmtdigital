using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMTDigital.Data.Models
{
	public sealed class Products
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ProductID { get; set; }

		[MaxLength(50)]
		public string ProductName { get; set; }

		[Column(TypeName = "decimal(9,2)")]
		public decimal? PackHeight { get; set; }

		[Column(TypeName = "decimal(9,2)")]
		public decimal? PackWidth { get; set; }

		[Column(TypeName = "decimal(8,3)")]
		public decimal? PackWeight { get; set; }

		[MaxLength(20)]
		public string Colour { get; set; }

		[MaxLength(20)]
		public string Size { get; set; }

		//SDW: Navigational Property
		public ICollection<OrderItems> OrderItems { get; set; } = new Collection<OrderItems>();
	}
}