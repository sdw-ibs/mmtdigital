using System.ComponentModel.DataAnnotations.Schema;
using KeyAttribute = System.ComponentModel.DataAnnotations.KeyAttribute;

namespace MMTDigital.Data.Models
{
	public sealed class OrderItems
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int OrderItemID { get; set; }

		[ForeignKey(nameof(Orders))]
		public int OrderID { get; set; }

		[ForeignKey(nameof(Products))]
		public int ProductID { get; set; }

		public int? Quantity { get; set; }

		[Column(TypeName = "decimal(9,2)")]
		public decimal? Price { get; set; }

		public bool? Returnable { get; set; }

		//SDW: Navigational Properties
		public Orders Orders { get; set; }

		public Products Products { get; set; }
	}
}