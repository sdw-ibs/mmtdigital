using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DateTime = System.DateTime;

namespace MMTDigital.Data.Models
{
	public sealed class Orders
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int OrderID { get; set; }

		[MaxLength(10)]
		public string CustomerID { get; set; }

		public DateTime? OrderDate { get; set; }

		public DateTime? DeliveryExpected { get; set; }

		public bool? ContainsGift { get; set; }

		[MaxLength(30)]
		public string ShippingMode { get; set; }

		[MaxLength(30)]
		public string OrderSource { get; set; }

		//SDW: Navigational Property
		public ICollection<OrderItems> OrderItems { get; set; } = new Collection<OrderItems>();
	}
}