﻿using DateTime = System.DateTime;

namespace MMTDigital.Domain.Models
{
	internal sealed class CustomerDetailsApiResponse
	{
		public string Email { get; set; }

		public string CustomerID { get; set; }

		public bool? Website { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public DateTime? LastLoggedIn { get; set; }

		public string HouseNumber { get; set; }

		public string Street { get; set; }

		public string Town { get; set; }

		public string Postcode { get; set; }

		public string PreferredLanguage { get; set; }
	}
}