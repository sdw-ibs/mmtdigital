﻿using System.Collections.Generic;
using DateTime = System.DateTime;
using Enumerable = System.Linq.Enumerable;

namespace MMTDigital.Domain.Models
{
	public sealed class Order
	{
		public int OrderNumber { get; set; }

		public DateTime? OrderDate { get; set; }

		public string DeliveryAddress { get; set; }

		public DateTime? DeliveryExpected { get; set; }

		public IEnumerable<OrderItem> OrderItems { get; set; } = Enumerable.Empty<OrderItem>();
	}
}