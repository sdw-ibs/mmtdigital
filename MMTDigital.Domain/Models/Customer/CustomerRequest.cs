﻿using System.ComponentModel.DataAnnotations;

namespace MMTDigital.Domain.Models
{
	public sealed class CustomerRequest
	{
		///<example>bob@mmtdigital.co.uk</example>
		[Required]
		[EmailAddress]
		public string User { get; set; }

		///<example>R223232</example>
		[Required]
		public string CustomerID { get; set; }
	}
}