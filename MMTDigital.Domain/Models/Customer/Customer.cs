﻿namespace MMTDigital.Domain.Models
{
	public sealed class Customer
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }
	}
}