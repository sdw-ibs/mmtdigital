﻿namespace MMTDigital.Domain.Models
{
	public sealed class CustomerResponse
	{
		public Customer Customer { get; set; }

		public Order Order { get; set; }
	}
}