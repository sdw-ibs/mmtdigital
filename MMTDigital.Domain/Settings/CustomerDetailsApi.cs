﻿namespace MMTDigital.Domain.Settings
{
	public sealed class CustomerDetailsApi
	{
		public string Uri { get; set; }

		public string Key { get; set; }
	}
}