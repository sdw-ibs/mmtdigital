﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Encoding = System.Text.Encoding;
using JsonConvert = Newtonsoft.Json.JsonConvert;
using MediaTypeWithQualityHeaderValue = System.Net.Http.Headers.MediaTypeWithQualityHeaderValue;

namespace MMTDigital.Domain.Services
{
	using Models;
	using Context = Data.Context;
	using CustomerDetailsApi = Settings.CustomerDetailsApi;

	//SDW: Encapsulates the bulk of this project's logic!
	public sealed class CustomerService : ICustomerService
	{
		#region Private
		private const string ApplicationJson = "application/json";

		private static readonly object Lock = new();

		private static Uri requestUri;

		private static HttpClient httpClient;

		private static void Initialize(CustomerDetailsApi customerDetailsApi)
		{
			requestUri = new Uri($"{customerDetailsApi.Uri}?code={customerDetailsApi.Key}");
			httpClient = new HttpClient();
			httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ApplicationJson));
		}

		private static string GetDeliveryAddress(CustomerDetailsApiResponse customerDetails) => string.Format
		(
			"{0} {1}, {2}, {3}",
			customerDetails.HouseNumber.ToUpperInvariant(),
			customerDetails.Street,
			customerDetails.Town,
			customerDetails.Postcode
		);

		private static async Task<CustomerDetailsApiResponse> GetCustomerDetailsApiResponseAsync(string email)
		{
			var request = new HttpRequestMessage
			{
				Method = HttpMethod.Post,
				RequestUri = requestUri,
				Content = new StringContent(JsonConvert.SerializeObject(new { email }), Encoding.UTF8, ApplicationJson)
			};
			var response = await httpClient.SendAsync(request);

			return response.IsSuccessStatusCode ?
				JsonConvert.DeserializeObject<CustomerDetailsApiResponse>(await response.Content.ReadAsStringAsync()) :
				throw new HttpRequestException($"Request [{request.Method} {request.RequestUri.GetLeftPart(UriPartial.Path)}] failed ({response.StatusCode}).", null, response.StatusCode);
		}

		private readonly Context Context;

		private async Task<Order> GetOrderAsync(CustomerDetailsApiResponse customerDetails)
		{
			var customerOrder = await Context.Orders
				.Where(x => x.CustomerID == customerDetails.CustomerID)
				.OrderByDescending(x => x.OrderDate)
				.FirstOrDefaultAsync();

			if (customerOrder == null)
				return null;

			var orderItems = await Context.OrderItems
				.Include(x => x.Products)
				.Where(x => x.OrderID == customerOrder.OrderID)
				.ToArrayAsync();

			return new Order
			{
				OrderNumber = customerOrder.OrderID,
				OrderDate = customerOrder.OrderDate,
				DeliveryAddress = GetDeliveryAddress(customerDetails),
				DeliveryExpected = customerOrder.DeliveryExpected,
				OrderItems = orderItems.Select
				(
					x => new OrderItem
					{
						Product = (customerOrder.ContainsGift == true) ? "Gift" : x.Products.ProductName,
						Quantity = x.Quantity,
						PriceEach = x.Price
					}
				)
				.ToArray()
			};
		}
		#endregion

		public CustomerService(Context context, IOptions<CustomerDetailsApi> options)
		{
			Context = context;

			//SDW: Double-checked locked initialization
			if (httpClient == null)
				lock (Lock)
					if (httpClient == null)
						Initialize(options.Value);
		}

		public async Task<CustomerResponse> GetOrderAsync(CustomerRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var customerDetails = await GetCustomerDetailsApiResponseAsync(request.User);

			return (customerDetails.CustomerID != request.CustomerID) ?
				throw new ArgumentException($"User's email [{request.User}] does not match customer number [{request.CustomerID}].") :
				new()
				{
					Customer = new()
					{
						FirstName = customerDetails.FirstName,
						LastName = customerDetails.LastName
					},
					Order = await GetOrderAsync(customerDetails)
				};
		}
	}
}