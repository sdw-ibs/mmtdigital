﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JsonConvert = Newtonsoft.Json.JsonConvert;
using HttpStatusCode = System.Net.HttpStatusCode;
using HttpRequestException = System.Net.Http.HttpRequestException;

namespace MMTDigital.Domain.Services
{
	using Models;

	//SDW: Mock customer service
	public sealed class CustomerServiceMock : ICustomerService
	{
		#region Private
		//SDW: Defines a test user in the test users JSON file
		private sealed class TestUser
		{
			public CustomerRequest CustomerRequest { get; set; }

			public CustomerResponse CustomerResponse { get; set; }
		}

		private static readonly object Lock = new();

		private static TestUser[] testUsers;
		#endregion

		public CustomerServiceMock(string testUsersJsonFile)
		{
			//SDW: Double-checked locked initialization
			if (testUsers == null)
				lock (Lock)
					if (testUsers == null)
						testUsers = File.Exists(testUsersJsonFile) ?
							JsonConvert.DeserializeObject<TestUser[]>(File.ReadAllText(testUsersJsonFile)) :
							throw new FileNotFoundException($"File [{testUsersJsonFile}] not found.");
		}

		public async Task<CustomerResponse> GetOrderAsync(CustomerRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var testUser = testUsers.SingleOrDefault(x => x.CustomerRequest.User == request.User);

			if (testUser == null)
				throw new HttpRequestException($"User [{request.User}] not found.", null, HttpStatusCode.NotFound);

			if (testUser.CustomerRequest.CustomerID != request.CustomerID)
				throw new ArgumentException($"User's email [{request.User}] does not match customer number [{request.CustomerID}].");

			if (testUsers == null)
				throw new ArgumentNullException(nameof(request));

			return await Task.Run(() => testUser.CustomerResponse);
		}
	}
}