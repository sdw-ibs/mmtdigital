﻿using System.Threading.Tasks;

namespace MMTDigital.Domain.Services
{
	using Models;

	public interface ICustomerService
	{
		Task<CustomerResponse> GetOrderAsync(CustomerRequest request);
	}
}